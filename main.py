import os
import re

import database

import praw
import prawcore
from ruamel import yaml

URL_REGEX = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*(),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'


def main():
    current_path = os.path.dirname(os.path.realpath(__file__))
    db = database.Database(os.path.join(current_path, 'data', 'data.db'))  # Open database connection

    with open(os.path.join(current_path, 'data', 'config.yml'), 'r') as f:
        config = yaml.load(f.read(), Loader=yaml.RoundTripLoader)

    contest_mode = config['data']['contest_mode']
    split_mode = config['settings']['split_mode']
    if split_mode:
        if contest_mode:
            mode = 'contest'
        else:
            mode = 'results'
    else:
        mode = 'mixed'

    reddit = praw.Reddit(
        username=config['auth']['username'],
        password=config['auth']['password'],
        client_id=config['auth']['client_id'],
        client_secret=config['auth']['client_secret'],
        user_agent=config['auth']['user_agent']
    )

    # Only subreddit moderators can use this script
    subreddit = reddit.subreddit(config['settings']['subreddit'])
    if config['auth']['username'] not in subreddit.moderator():
        return

    first_contest = not db.history_count

    old_submission = None
    comments = []
    if not first_contest:
        url = db.last_contest_url
        old_submission = reddit.submission(url=url)
        old_submission.comments.replace_more(limit=0)

        # Fetch the top 3 upvoted comments with links
        if not contest_mode or not split_mode:
            for comment in old_submission.comments:
                urls = re.findall(URL_REGEX, comment.body)
                if urls and not comment.removed:
                    comments.append({'score': comment.score, 'author': comment.author.name, 'link': urls[0]})

    top_comments = sorted(comments, key=lambda comment: comment['score'], reverse=True)[:3]

    themes_file = os.path.join(current_path, 'data', 'themes.txt')
    if not os.path.exists(themes_file):
        open(themes_file, 'w').close()

    with open(themes_file, 'r') as file:
        lines = file.readlines()
        if len(lines) < 2:
            # At least 2 themes are required to run this script
            print('Add at least 2 themes to the themes.txt file')
            return

        theme, description = lines[0].split(':', 1)
        next_theme, next_description = lines[1].split(':', 1)
        previous_theme = lines[-1].split(':', 1)[0]

    keywords = {
        'theme_name': theme,
        'theme_description': description,
        'next_theme_name': next_theme,
        'next_theme_description': next_description,
        'previous_theme_name': previous_theme,
        'contest_number': config['data']['contest_number']
    }

    # Replace temp keywords with variables
    texts = {}
    for key, text in config['text'][mode].items():
        text = text.format(**keywords)
        texts[key] = text
    texts['bot_text'] = config['text']['bot_text'].format(**keywords)

    # Draft the body
    body = [texts['upper_text']]
    if not first_contest and (not contest_mode or not split_mode):
        if top_comments:
            body.append(texts['winners_text'] + '\n')
            for index, comment in enumerate(top_comments):
                user = comment['author']
                placement = index + 1
                updated_points = 0
                if config['settings']['point_mode']:
                    points = config['settings']['points'][placement]
                    updated_points = db.add_user_points(user, points)

                    # Update flair
                    if db.user_allows_custom_flair(user):
                        tiers = dict(config['settings']['tiers'])
                        point_requirements = sorted(tiers.keys(), reverse=True)
                        tier_key = None
                        for points in point_requirements:
                            if updated_points >= points:
                                tier_key = points
                                break
                        if tier_key is None:
                            continue

                        flair_name = tiers[tier_key]['name']
                        flair_class = tiers[tier_key]['class']
                        flair = '{} ({} points)'.format(flair_name, updated_points)
                        subreddit.flair.set(user, flair, flair_class)

                data = {
                    'ordinal': ordinal(placement),
                    'user': user,
                    'link': comment['link']
                }
                if config['settings']['point_mode']:
                    data['points'] = '({} points)'.format(updated_points)
                    body.append('- {ordinal} Place: /u/{user} {points} {link} \n'.format(**data))
                else:
                    body.append('- {ordinal} Place: /u/{user} {link} \n'.format(**data))
        else:
            body.append(texts['no_winners_text'] + '\n')

    body.append(texts['bottom_text'])
    body.append(texts['bot_text'])
    body = ''.join(body)
    body = body.replace('\n', '\n\n')  # Double the line breaks because of reddit formatting

    # Try to unsticky and remove contest mode from the old contest
    if not first_contest:
        try:
            old_submission.mod.sticky(False)
            old_submission.mod.contest_mode(False)
        except prawcore.exceptions.BadRequest:
            pass

    subreddit = reddit.subreddit(config['settings']['subreddit'])
    submission = subreddit.submit(texts['title'], body, send_replies=False)  # Submit the post

    if contest_mode or not split_mode:
        submission.mod.contest_mode()

    if config['settings']['sticky_thread']:
        submission.mod.sticky()

    if not contest_mode or not split_mode:
        # Cycle the themes
        with open(themes_file, 'r+') as file:
            lines = file.read().splitlines()
            lines.append(lines.pop(0))
            file.seek(0)
            file.write('\n'.join(lines))

    # Add this submission to the history file
    db.last_contest_url = (submission.url, mode)

    if mode == 'contest':
        config['data']['contest_mode'] = False
    elif mode == 'results':
        config['data']['contest_mode'] = True

    # Update contest number
    if contest_mode or not split_mode:
        config['data']['contest_number'] += 1

    with open(os.path.join(current_path, 'data', 'config.yml'), 'w') as file:
        yaml.dump(config, file, Dumper=yaml.RoundTripDumper)


def ordinal(number):
    return str(number) + 'tsnrhtdd'[number % 5 * (number % 100 ^ 15 > 4 > number % 10)::4]


if __name__ == "__main__":
    main()
