import sqlite3


class Database:

    def __init__(self, database):
        """Initialize a new or connect to an existing database.
        """
        self.database = database
        self.connection = sqlite3.connect(self.database)
        self.cursor = self.connection.cursor()

        self._create_tables()

    def _create_tables(self):
        """ Creates all needed tables
        :return:
        """
        create_history_table_sql = """ 
            CREATE TABLE IF NOT EXISTS history (
                id INTEGER PRIMARY KEY,
                url TEXT,
                mode TEXT,
                timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
            ); """
        create_users_table_sql = """ 
            CREATE TABLE IF NOT EXISTS users (
                id INTEGER PRIMARY KEY,
                user TEXT,
                points INT,
                custom_flair INT DEFAULT 1
            ); """

        self.cursor.execute(create_history_table_sql)
        self.cursor.execute(create_users_table_sql)

    @property
    def history_count(self):
        cursor = self.cursor
        cursor.execute('SELECT id FROM history')
        count = 0
        for _ in cursor:
            count += 1
        return count

    @property
    def last_contest_url(self):
        cursor = self.cursor
        cursor.execute('SELECT url FROM history ORDER BY id DESC LIMIT 1')
        result = cursor.fetchone()
        if result:
            return result[0]
        return 0

    @last_contest_url.setter
    def last_contest_url(self, row):
        cursor = self.cursor
        cursor.execute("INSERT INTO history (url, mode) VALUES (?, ?)", row)
        self.connection.commit()

    def get_user_points(self, user):
        cursor = self.cursor
        cursor.execute("SELECT points FROM users WHERE user=?", [user])
        result = cursor.fetchone()
        if result:
            return result[0]
        return None

    def add_user_points(self, user, points):
        cursor = self.cursor
        current_points = self.get_user_points(user)
        if current_points is not None:
            points = current_points + points
            cursor.execute("UPDATE users SET points=? WHERE user=?", [points, user])
        else:
            cursor.execute("INSERT INTO users (user, points) VALUES (?, ?)", [user, points])
        self.connection.commit()

        return points

    def user_allows_custom_flair(self, user):
        cursor = self.cursor
        cursor.execute("SELECT custom_flair FROM users WHERE user=?", [user])
        result = cursor.fetchone()
        if result:
            return bool(result[0])
        return None
